FROM golang:buster

EXPOSE 80
ENV GOPATH=/usr GOBIN=/usr/bin
RUN go get golang.org/x/tools/cmd/present

WORKDIR /usr/src/HelloPresent
COPY . .

ENTRYPOINT ["/usr/bin/present"]
CMD ["-notes", "-http", ":80"]
